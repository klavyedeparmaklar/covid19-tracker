import React, { useState, useEffect } from 'react'

import { fetchDailyData } from '../../api'

// Chart Lib
import { Line, Bar, Doughnut } from 'react-chartjs-2'

// CSS
import styles from './Chart.module.css'
import { Card, CardContent, Grid } from '@material-ui/core'
import cx from 'classnames'

const Chart = ({ data: { confirmed, recovered, deaths }, country }) => {
    const [dailyData, setDailyData] = useState([])
    useEffect(() => {
        const fetchAPI = async () => {
            setDailyData(await fetchDailyData())
        }
        fetchAPI()
    }, [])

    const lineChart = (
        dailyData.length ? (<Line
            data={{
                labels: dailyData.map(({ date }) => date),
                datasets: [
                    {
                        data: dailyData.map(({ confirmed }) => confirmed),
                        label: 'Infected',
                        borderColor: '#3333ff',
                        fill: true
                    },
                    {
                        data: dailyData.map(({ deaths }) => deaths),
                        label: 'Deaths',
                        borderColor: 'red',
                        backgroundColor: 'rgba(255,0,0,0.5)',
                        fill: true
                    }]
            }}
        />) : null
    )

    const barChart = (
        confirmed ? (
            <Bar
                data={{
                    labels: ['Infected', 'Recoverd', 'Deaths'],
                    datasets: [{
                        label: 'People',
                        backgroundColor: [
                            'rgba(0,0,255,0.5)',
                            'rgba(0,255,0,0.5)',
                            'rgba(255,0,0,0.5)'
                        ],
                        data: [confirmed.value, recovered.value, deaths.value]
                    }]
                }}
                options={{
                    legend: { display: false },
                    title: { display: true, text: `Current state in ${country}` }
                }}
            />
        ) : null
    )

    const doughnutChart = (
        confirmed ? (
            <Doughnut
                width={"100vh"}
                data={{
                    labels: ['Infected', 'Recoverd', 'Deaths'],
                    datasets: [{
                        label: 'People',
                        backgroundColor: [
                            'rgba(0,0,255,0.5)',
                            'rgba(0,255,0,0.5)',
                            'rgba(255,0,0,0.5)'
                        ],
                        data: [confirmed.value, recovered.value, deaths.value]
                    }]
                }}
                options={{
                    legend: { display: false }
                }}
            />
        ) : null
    )

    return (
        <div className={styles.container}>
            {country ? (
                <Grid container spacing={2} jutify='center'>
                    <Grid item className={cx(styles.card, styles.barChart)}>
                        <CardContent>
                            {barChart}
                        </CardContent>
                    </Grid>
                    <Grid item className={cx(styles.card, styles.doughnutChart)}>
                        <CardContent>
                            {doughnutChart}
                        </CardContent>
                    </Grid>
                </Grid>
            ) : lineChart}
        </div>
    )
}

export default Chart